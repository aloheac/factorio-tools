# assembly-planner

The assemply-planner python scripts helps to determine how much assembly
machines we need to produce a resource at a specified frequency.

## Usage

### Edit the resources present in the bus

They are listed in the main function and this needs to match your factory.

These are the resources already produced elsewhere and that can be provided to
the future assembly machines without the need to be produced locally.

### Update the recipes

Recipes are the main part of the script, they are objects defined this way:

```python
<resourceName> = ProductedResource(
    "<Resource Name>",
    <recipe duration>,
    <quantity produced>,
    [(<requiredResource>, <requiredQuantity>),
     (<otherRequiredResource>, <requiredQuantity>),
     ...]
)
```

For example:

```python
utilitySciencePack = ProductedResource(
    "Utility Science Pack",
    21,
    3,
    [(processingUnit, 2),
     (flyingRobotPart, 1),
     (lowDensityStructure, 3)]
)

```

Make sure all the required recipes are defined for your needs.

If resources are in the bus, there is no need to specify the details as they
won't need assemblers anyway (duration can be null, quantity set to 1 and
requirements can be empty).

For example:

```python
plasticBar = ProductedResource("Plastic Bar", 0, 1, [])
```

Although the bus resources doesn't need to be detailed, there is no need to
remove recipes if they are provided/on the bus: each game doesn't have the same
resources on the bus!

### Ask what you need

In the main function, replace the resource to be produed and it's frequency with
what you want.

The frequency is often 1 (i.e. 1 resource per second) but this is not fixed.

### Run the program

```bash
./assembly-planner.py
```

This will produce output like this:

```
 7.00 * Production Science Pack (21.00s)
            1 Electric Furnace
            1 Productivity Module
           30 Rail
 1.67 * Electric Furnace (5.00s)
            5 Advanced Circuit
           10 Steel Plate
           10 Stone Brick
20.00 * Advanced Circuit (6.00s)
            2 Plastic Bar
            4 Copper Wire
            2 Electronic Circuit
 9.58 * Copper Wire (0.50s)
            1 Copper Plate
 4.17 * Electronic Circuit (0.50s)
            1 Iron Plate
            3 Copper Wire
 5.00 * Productivity Module (15.00s)
            5 Electronic Circuit
            5 Advanced Circuit
 2.50 * Rail (0.50s)
            1 Iron Stick
            1 Steel Plate
            1 Stone
 1.25 * Iron Stick (0.50s)
            1 Iron Plate
```

This show which resources need to be produced, how much assembly machines they
need, the time of the recipe and the resources each assembly will need.

The really usefull data is the number of assembly machine needed, the recipe
duration and resources required is used to determine how fast the required
resources should be loaded.