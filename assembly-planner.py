#!/usr/bin/env python3

from typing import List, Tuple, Dict
from pprint import pprint

# This represents a resource that can be crafted from other resources
class ProductedResource:
    def __init__(self, name: str,
                 duration: float,
                 quantity: int,
                 req: List[Tuple['ProductedResource', float]]):
        # Human readable name
        self.name = name
        # Time to produce the resource
        self.duration = duration
        # Quantity producted, often 1
        self.quantity = quantity
        # List of quantity/resources required
        self.req = req

# Resources provided by the bus, no need for details there
ironPlate = ProductedResource("Iron Plate", 0, 1, [])
copperPlate = ProductedResource("Copper Plate", 0, 1, [])
steelPlate = ProductedResource("Steel Plate", 0, 1, [])
plasticBar = ProductedResource("Plastic Bar", 0, 1, [])
lubricant = ProductedResource("Lubricant", 0, 1, [])
sulfur = ProductedResource("Sulfur", 0, 1, [])
water = ProductedResource("Water", 0, 1, [])
stone = ProductedResource("Stone", 0, 1, [])
stoneBrick = ProductedResource("Stone Brick", 0, 1, [])

pipe = ProductedResource(
    "Pipe",
    0.5,
    1,
    [(ironPlate, 1)]
)

ironGearWheel = ProductedResource(
    "Iron Gear Wheel",
    0.5,
    1,
    [(ironPlate, 2)]
)

firearmMagazine = ProductedResource(
    "Firearm Magazine",
    1,
    1,
    [(ironPlate, 4)]
)

piercingRoundMagazine = ProductedResource(
    "Piercing Round Magazine",
    3,
    1,
    [(copperPlate, 5),
     (steelPlate, 1),
     (firearmMagazine, 1)]
)

copperWire = ProductedResource(
    "Copper Wire",
    0.5,
    2,
    [(copperPlate, 1)]
)

electronicCircuit = ProductedResource(
    "Electronic Circuit",
    0.5,
    1,
    [(ironPlate, 1),
     (copperWire, 3)]
)

advancedCircuit = ProductedResource(
    "Advanced Circuit",
    6,
    1,
    [(plasticBar, 2),
     (copperWire, 4),
     (electronicCircuit, 2)]
)

sulfuricAcid = ProductedResource(
    "Surlfuric Acid",
    1,
    50,
    [(ironPlate, 1),
     (sulfur, 5),
     (water, 100)]
)

processingUnit = ProductedResource(
    "Processing Unit",
    10,
    1,
    [(electronicCircuit, 20),
     (advancedCircuit, 2),
     (sulfuricAcid, 5)]
)

battery = ProductedResource(
    "Battery",
    4,
    1,
    [(ironPlate, 1),
     (copperPlate, 1),
     (sulfuricAcid, 20)]
)

engineUnit = ProductedResource(
    "Engine Unit",
    10,
    1,
    [(steelPlate, 1),
     (ironGearWheel, 1),
     (pipe, 2)]
)

electricEngineUnit = ProductedResource(
    "Electric Engine Unit",
    10,
    1,
    [(electronicCircuit, 2),
     (engineUnit, 1),
     (lubricant, 15)]
)

flyingRobotPart = ProductedResource(
    "Flying Robot Part",
    20,
    1,
    [(steelPlate, 1),
     (battery, 2),
     (electronicCircuit, 3),
     (electricEngineUnit, 1)]
)

lowDensityStructure = ProductedResource(
    "Low Density Structure",
    20,
    1,
    [(copperPlate, 20),
     (steelPlate, 2),
     (plasticBar, 5)]
)

utilitySciencePack = ProductedResource(
    "Utility Science Pack",
    21,
    3,
    [(processingUnit, 2),
     (flyingRobotPart, 1),
     (lowDensityStructure, 3)]
)

electricFurnace = ProductedResource(
    "Electric Furnace",
    5,
    1,
    [(advancedCircuit, 5),
     (steelPlate, 10),
     (stoneBrick, 10)]
)

productivityModule = ProductedResource(
    "Productivity Module",
    15,
    1,
    [(electronicCircuit, 5),
     (advancedCircuit, 5)]
)

ironStick = ProductedResource(
    "Iron Stick",
    0.5,
    2,
    [(ironPlate, 1)]
)

rail = ProductedResource(
    "Rail",
    0.5,
    2,
    [(ironStick, 1),
     (steelPlate, 1),
     (stone, 1)]
)

productionSciencePack = ProductedResource(
    "Production Science Pack",
    21,
    3,
    [(electricFurnace, 1),
     (productivityModule, 1),
     (rail, 30)]
)


# Returns the number of assembly machines necessary to produce f resources per
# seconds
def getAssemblyMachineNr(pr: ProductedResource, f: float) -> float:
    n = f * pr.duration / pr.quantity
    return n

# Add the resource and the associated number of assembly machine to the result
# dictionary
def addToDict(d: Dict[str, Tuple[float, ProductedResource]],
              name: str,
              qty: float,
              pr: ProductedResource) -> None:
    if name not in d:
        d[name] = (qty, pr)
    else:
        value = d[name]
        d[name] = (value[0] + qty, value[1])

# Fill the Dict d with the neccessary number of assembly machine per resources
# to be produced to get f resources per seconds. Each element in the Dict bus
# won't be produced locally, this assumes they are produced elsewhere
def getAssemblyMachineLayout(pr: ProductedResource,
                             f: float,
                             bus: Dict[str, int],
                             d: Dict[str, Tuple[float, ProductedResource]]) -> None:
    nbA = getAssemblyMachineNr(pr, f)
    addToDict(d, pr.name, nbA, pr)

    for req in pr.req:
        reqPr = req[0]
        reqQty = req[1]

        if reqPr.name not in bus:
            f = nbA * reqQty / pr.duration
            getAssemblyMachineLayout(reqPr, f, bus, d)

# Display the number of assembly machines required to produce a resource
def printResult(d: Dict[str, Tuple[float, ProductedResource]],
                bus: Dict[str, int]):
    for t in d:
        nr = d[t][0]
        pr = d[t][1]
        print("%5.2f * %s (%3.2fs)" % (nr, pr.name, pr.duration))

        for req in pr.req:
            name = req[0].name
            qty = req[1]
            print("          %3d %s" % (qty, name))

if __name__ == "__main__":
    # Resources present on the bus that don't have to be produced
    bus = {
        "Iron Plate": 1,
        "Copper Plate": 1,
        "Steel Plate": 1,
        "Plastic Bar": 1,
        "Lubricant": 1,
        "Sulfur": 1,
        "Water": 1,
        "Stone": 1,
        "Stone Brick": 1,
    }

    # Dictionary filled with number of assembly machines
    d = {} #type: Dict[str, Tuple[float, ProductedResource]]
    getAssemblyMachineLayout(productionSciencePack, 1, bus, d)
    printResult(d, bus)

